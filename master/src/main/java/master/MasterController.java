package master;

import lombok.extern.log4j.Log4j2;
import master.models.CommitResult;
import master.models.FileResult;
import master.models.requests.InitRequest;
import master.models.responses.TaskResponse;
import master.models.responses.WholeCommitResponse;
import master.services.MasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;

import java.net.URI;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@Log4j2
public class MasterController {

    @Autowired
    @Qualifier("Slave")
    private MasterService service;

    @RequestMapping(method = POST, value = "/init")
    public ResponseEntity<Object> init(@RequestBody @Valid InitRequest request) {
        this.service.init(request.getRepositoryPath(), request.getName());
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{name}")
                .buildAndExpand(request.getName()).toUri();
        return ResponseEntity.created(location).build();
    }

    @RequestMapping(method = GET, value = "/repository")
    public TaskResponse getTask() {
        return this.service.getTask();
    }

    @RequestMapping(method = POST, value = "/repository")
    public void postResult(@RequestBody @Valid FileResult result) {
        this.service.processResult(result);
    }

    @RequestMapping(method = GET, value = "/greeting")
    public String greeting() {
        log.info("Greeting...");
        return "hello";
    }
}
