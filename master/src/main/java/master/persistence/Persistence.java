package master.persistence;

import master.services.MasterService;

public interface Persistence<T extends MasterService> {
    void save(T service);
}
