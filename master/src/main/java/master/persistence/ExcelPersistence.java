package master.persistence;

import lombok.extern.log4j.Log4j2;
import master.models.CommitHistory;
import master.services.MasterService;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Map;

@Log4j2
@Component
public class ExcelPersistence<T extends MasterService> implements Persistence {

    private static final String FILE_EXTENSION = ".xlsx";
    private static String[] headers = new String[] { "Commit Hash", "Result", "Time (ms)" };

    @Override
    public void save(final MasterService service) {
        final String fileName = String.format("%s - (%s)%s",
                service.getName(), new Timestamp(System.currentTimeMillis()), ExcelPersistence.FILE_EXTENSION);
        final String sheetName = "Results " + service.getClass().getSimpleName();
        log.info(String.format("Saving file %s", fileName));

        final XSSFWorkbook workbook = new XSSFWorkbook();
        final Sheet sheet = workbook.createSheet(sheetName);

        // Populate Headers.
        Row headerRow = sheet.createRow(0);
        for (int cellIndex = 0; cellIndex < this.headers.length; cellIndex++) {
            headerRow.createCell(cellIndex).setCellValue(this.headers[cellIndex]);
        }

        // Populate Commit Information.
        int rowCounter = 1;
        for (Map.Entry<String, CommitHistory> entry : service.getHistory().entrySet()) {
            Row row = sheet.createRow(rowCounter++);

            row.createCell(0).setCellValue(entry.getKey());
            row.createCell(1).setCellValue(entry.getValue().getResult().sum());
            row.createCell(2).setCellValue(entry.getValue().getFinalTime() - entry.getValue().getInitialTime());
        }

        // Populate Total Information
        Row totalRow = sheet.createRow(rowCounter);
        totalRow.createCell(0).setCellValue(service.getName());
        totalRow.createCell(1).setCellValue(service.getTotalResult());
        totalRow.createCell(2).setCellValue(service.getFinishedTime() - service.getStartedTime());

        FileOutputStream fileStream = null;
        try {
            fileStream = new FileOutputStream(fileName);
        } catch (FileNotFoundException e) {
            log.warn(e);
        }

        try {
            workbook.write(fileStream);
            fileStream.flush();
            fileStream.close();
        } catch (IOException e) {
            log.warn(e);
        }

        log.info(String.format("Saved file %s", fileName));
    }
}
