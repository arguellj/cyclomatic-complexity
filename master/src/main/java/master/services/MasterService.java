package master.services;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import master.models.CommitHistory;
import master.models.FileResult;
import master.models.responses.AwakeWorkerRequest;
import master.models.responses.TaskResponse;
import master.persistence.Persistence;
import org.eclipse.jgit.diff.DiffEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import versioncontrol.VersionControl;
import versioncontrol.models.FileChange;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

/**
 * Service of master node.
 */
@Configuration
@Log4j2
public abstract class MasterService {

    @Autowired
    Persistence<MasterService> persistenceManager;

    /**
     * Version Control repository manager.
     */
    @Autowired
    VersionControl repository;

    /**
     * Base URL of the available workers.
     */
    @Value("#{'${app.workers}'.split(',')}")
    List<String> workers = new ArrayList<>();

    /**
     * Map to keep the history of the processing of all the commits of the repository.
     *  Key:         Value:
     *  CommitId -> {@link CommitHistory}
     */
    @Getter
    ConcurrentHashMap<String, CommitHistory> history = new ConcurrentHashMap<>();

    /**
     * Counter of commits completed in order to know when to process the calculation of all the commits.
     */
    CountDownLatch commitsCompleted;

    /**
     * Time when the repository processing started.
     */
    @Getter
    long startedTime;

    /**
     * Time when the repository processing finished.
     */
    @Getter
    long finishedTime;

    /**
     * Total Result of all the commits.
     */
    @Getter
    double totalResult;

    /**
     * Unique name for the execution.
     */
    @Getter
    String name;

    /**
     * Initialize the master node.
     * @param repositoryPath the repository to clone and process
     * @param name an unique name id to identify the current execution.
     */
    public void init(final String repositoryPath, final String name) {
        this.startedTime = System.currentTimeMillis();
        this.name = name;
        this.repository.cloneRepository(repositoryPath, name);
        CompletableFuture.runAsync(this::processRepository);
        CompletableFuture.runAsync(() -> this.wakeNodes(repositoryPath, name));
    }

    /**
     * Process the result given in {@param fileResult}.
     * @param fileResult the result of processing the file.
     */
    public void processResult(final FileResult fileResult) {
        log.debug(String.format("Result of file '%s' in commit '%s' with result: '%f'.",
                fileResult.getFileName(), fileResult.getCommitHashCode(), fileResult.getResult()));

        this.history.get(fileResult.getCommitHashCode()).logResult(fileResult);

        if (this.history.get(fileResult.getCommitHashCode()).isCompleted()) {
            // Mark as Completed.
            this.history.get(fileResult.getCommitHashCode()).setFinalTime(System.currentTimeMillis());
            this.commitsCompleted.countDown();
            log.info(String.format("Finalized commit %s, left %d commits",
                    fileResult.getCommitHashCode(), this.commitsCompleted.getCount()));
        }
    }

    /**
     * Get a pending task.
     * @return the pending task.
     */
    public abstract TaskResponse getTask();

    /**
     * Process all the files in all the commits.
     */
    final void processRepository() {
        log.info("Process repository...");
        Stack<String> commitIds = this.repository.getAllCommitIds();

        this.commitsCompleted = new CountDownLatch(commitIds.size());

        CommitHistory previousCommit = null;
        while (!commitIds.empty()) {
            String currentCommitId = commitIds.pop();
            log.debug("Processing commit " + currentCommitId);
            this.repository.checkoutRepository(currentCommitId);

            // Get files to process.
            Set<FileChange> fileChanges = this.repository.getFilesChanged();

            // Create commit history.
            CommitHistory commitHistory = new CommitHistory(previousCommit);
            commitHistory.addFilesChanges(fileChanges);
            this.history.put(currentCommitId, commitHistory);
            previousCommit = commitHistory;

            // Do not process files that were deleted.
            Set<FileChange> filesToProcessed = fileChanges
                    .stream()
                    .filter(f -> f.getType() != DiffEntry.ChangeType.DELETE)
                    .collect(Collectors.toSet());

            this.processFiles(filesToProcessed, currentCommitId);
        }

        this.reduce();
        this.save();
    }

    final void wakeNodes(final String repositoryPath, final String name) {
        log.info("Awaking workers...");
        AwakeWorkerRequest request = new AwakeWorkerRequest(repositoryPath, name);

        this.workers.parallelStream().forEach(workerBaseUrl -> {
            final String uri = workerBaseUrl + "/init";
            log.info(String.format("Awaking %s", uri));
            RestTemplate restTemplate = new RestTemplate();
            CompletableFuture.runAsync(() -> restTemplate.postForLocation(uri, request));
        });
    }

    void reduce() {
        try {
            log.info("Waiting for all commits to be processed.");
            this.commitsCompleted.await();
        } catch (InterruptedException e) {
            log.error(e);
        }

        log.info("All the commits has been processed. Reducing sub results.");
        final Stack<String> allCommitIds = this.repository.getAllCommitIds();
        while (!allCommitIds.empty()) {
            String commitId = allCommitIds.pop();
            log.debug(String.format("Reducing commit '%s'", commitId));
            this.history.get(commitId).reduce();
        }

        this.totalResult = this.history.values().stream().mapToDouble(h -> h.getResult().sum()).sum();
        this.finishedTime = System.currentTimeMillis();

        log.info(String.format("The execution has finished in '%d' ms with result '%f'",
                this.finishedTime - this.startedTime, this.totalResult));

        // TODO: Delete
        this.history.entrySet().stream().forEach(e -> {
            log.info(String.format("Commit %s finished with result %f in %d ms.",
                    e.getKey(), e.getValue().getResult().sum(), e.getValue().getFinalTime() - e.getValue().getInitialTime()));
        });
    }

    void save() {
        this.persistenceManager.save(this);
    }

    abstract void processFiles(final Set<FileChange> filesChanges, final String currentCommitId);

}
