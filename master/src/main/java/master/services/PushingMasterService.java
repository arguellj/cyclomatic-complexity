package master.services;

import lombok.extern.log4j.Log4j2;
import master.models.responses.FileTaskResponse;
import master.models.responses.TaskResponse;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import versioncontrol.models.FileChange;

import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Work-Pushing communication pattern.
 * The master process pushes the computations to be done to the message queues of the slave processes,
 * which will process them one by one and push the partial results back to the master process.
 */
@Service
@Log4j2
@Qualifier("Pushing")
public class PushingMasterService extends MasterService {
    @Override
    public TaskResponse getTask() {
        return null;
    }

    @Override
    void processFiles(Set<FileChange> filesChanges, String currentCommitId) {
        log.debug("Spawning threads...");

        if (!this.history.get(currentCommitId).getHasStarted().getAndSet(true)) {
            // Set time when commit start to be processed by the first thread.
            if (filesChanges.isEmpty()) {
                this.history.get(currentCommitId).setInitialTime(0);
                this.history.get(currentCommitId).setFinalTime(0);
                this.commitsCompleted.countDown();
                log.debug(String.format("Finalized commit %s, left %d commits",
                        currentCommitId, this.commitsCompleted.getCount()));
                return;
            } else {
                this.history.get(currentCommitId).setInitialTime(System.currentTimeMillis());
            }
        }

        AtomicInteger workerIndex = new AtomicInteger(0);
        filesChanges.parallelStream()
                .forEach(file -> {
                    String uri = this.workers.get(workerIndex.getAndIncrement() % this.workers.size()) + "/process";
                    FileTaskResponse newTask = new FileTaskResponse(currentCommitId, file.getFileName());
                    RestTemplate restTemplate = new RestTemplate();
                    restTemplate.postForLocation(uri, newTask);
                });
    }
}
