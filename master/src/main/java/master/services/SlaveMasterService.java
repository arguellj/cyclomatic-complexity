package master.services;

import lombok.extern.log4j.Log4j2;
import master.models.FileResult;
import master.models.responses.FileTaskResponse;
import master.models.responses.TaskResponse;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import versioncontrol.models.FileChange;

import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Master-Slave communication pattern.
 * A single master process spawns a bunch of slave processes to do computations on other nodes,
 * and then combines the results.
 */
@Service
@Log4j2
@Qualifier("Slave")
public class SlaveMasterService extends MasterService {

    @Override
    public void init(String repositoryPath, String name) {
        super.init(repositoryPath, name);
    }

    @Override
    public TaskResponse getTask() {
        return null;
    }

    @Override
    protected void processFiles(Set<FileChange> filesChanges, String currentCommitId) {
        log.debug("Spawning threads...");

        if (!this.history.get(currentCommitId).getHasStarted().getAndSet(true)) {
            // Set time when commit start to be processed by the first thread.
            if (filesChanges.isEmpty()) {
                this.history.get(currentCommitId).setInitialTime(0);
                this.history.get(currentCommitId).setFinalTime(0);
                this.commitsCompleted.countDown();
                log.debug(String.format("Finalized commit %s, left %d commits",
                        currentCommitId, this.commitsCompleted.getCount()));
                return;
            }
            this.history.get(currentCommitId).setInitialTime(System.currentTimeMillis());
        }

        AtomicInteger workerIndex = new AtomicInteger(0);
        filesChanges.parallelStream()
                .forEach(file -> {
                    String uri = this.workers.get(workerIndex.getAndIncrement() % this.workers.size()) + "/process";
                    FileTaskResponse newTask = new FileTaskResponse(currentCommitId, file.getFileName());
                    RestTemplate restTemplate = new RestTemplate();
                    restTemplate.postForLocation(uri, newTask);
                });
    }
}
