package master.services;

import lombok.extern.log4j.Log4j2;
import master.models.responses.FileTaskResponse;
import master.models.responses.TaskResponse;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import versioncontrol.models.FileChange;

import java.sql.Timestamp;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Work-Stealing communication pattern.
 * A single master node and a bunch of slave nodes.
 * Each of the slave nodes runs a single slave process.
 * The master node does not push work to the slaves, but rather the slaves query the master for work.
 */
@Service
@Log4j2
@Qualifier("Stealing")
public class StealingMasterService extends MasterService {

    private LinkedBlockingQueue<FileTaskResponse> pendingJobs = new LinkedBlockingQueue<>();

    @Override
    public TaskResponse getTask() {
        log.debug("Getting new Task");
        FileTaskResponse response = null;
        try {
            response = this.pendingJobs.poll(30, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            log.warn(e);
        }

        if (response != null) {
            if (!this.history.get(response.getCommitHashCode()).getHasStarted().getAndSet(true)) {
                // Set time when commit start to be processed by the first thread.
                this.history.get(response.getCommitHashCode()).setInitialTime(System.currentTimeMillis());
            }

            log.debug(String.format("Sending task file: '%s' - commit: '%s'",
                    response.getFileName(), response.getCommitHashCode()));
        } else {
            log.info("Terminating workers...");
            response = new FileTaskResponse("", "");
        }

        return response;
    }

    @Override
    protected void processFiles(Set<FileChange> filesChanges, String currentCommitId) {
        // Fill Queue with jobs
        log.debug("Enqueueing files of commit " + currentCommitId);
        if (filesChanges.isEmpty()) {
            this.history.get(currentCommitId).setInitialTime(0);
            this.history.get(currentCommitId).setFinalTime(0);
            this.commitsCompleted.countDown();
            log.debug(String.format("Finalized commit %s, left %d commits",
                    currentCommitId, this.commitsCompleted.getCount()));
            return;
        }

        filesChanges.parallelStream()
                .forEach(file -> {
                    FileTaskResponse newTask = new FileTaskResponse(currentCommitId, file.getFileName());
                    try {
                        this.pendingJobs.put(newTask);
                    } catch (InterruptedException e) {
                        log.error(e);
                    }
                });
    }
}
