package master.models;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import versioncontrol.models.FileChange;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.DoubleAdder;

@Log4j2
public class CommitHistory {

    /**
     * Reference to previous commit.
     */
    private CommitHistory previousCommit;

    /**
     *
     */
    @Getter
    private AtomicBoolean hasStarted;

    /**
     * Time when the commit started to be processed.
     */
    @Getter
    @Setter
    private long finalTime;

    /**
     * Time when the commit finished to be processed.
     */
    @Getter
    @Setter
    private long initialTime;

    /**
     * History of all the files processed on this commit.
     * Key          Value
     * FileName -> {@link FileHistory }
     */
    @Getter
    private ConcurrentHashMap<String, FileHistory> filesToProcessed;

    /**
     * Result of the commit given by the sum of all the files on this commit.
     */
    @Getter
    private DoubleAdder result;

    public CommitHistory(CommitHistory previousCommit) {
        this.previousCommit = previousCommit;
        this.result = new DoubleAdder();
        this.filesToProcessed = new ConcurrentHashMap();
        this.hasStarted = new AtomicBoolean(false);
    }

    /**
     * Check if the commit has been processed completely.
     * @return
     */
    public synchronized boolean isCompleted() {
        return this.filesToProcessed.isEmpty()
                || this.filesToProcessed.values().stream().allMatch(v -> v.isHasBeenProcessed());
    }

    /**
     * Logs the result of one file processing.
     * @param result the result of the file processed
     */
    public synchronized void logResult(FileResult result) {
        this.filesToProcessed.get(result.getFileName()).setResult(result.getResult());
        this.filesToProcessed.get(result.getFileName()).setHasBeenProcessed(true);
        this.result.add(result.getResult());
    }

    /**
     * Adds the files changed to be processed on this commit.
     * @param changes files to be processed.
     */
    public void addFilesChanges(Set<FileChange> changes) {
        for (FileChange change : changes) {
            // Marked as processed when the change is DELETE.
            FileHistory fileHistory = new FileHistory(change.getFileName(), change.getType());
            this.filesToProcessed.put(change.getFileName(), fileHistory);
        }
    }

    /**
     * Reduce the result of all files to a total result for the commit.
     */
    public void reduce() {
        if (this.previousCommit != null) {
            this.previousCommit.getFilesToProcessed().keySet().removeAll(this.filesToProcessed.keySet());
            this.result.add(this.previousCommit.getFilesToProcessed().values()
                    .stream()
                    .mapToDouble(f -> f.getResult())
                    .sum());
        }
    }
}
