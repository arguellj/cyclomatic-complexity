package master.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class CommitResult {

    @Getter
    private String commitHashCode;

    @Getter
    private double result;

    @Override
    public int hashCode() {
        return commitHashCode.hashCode();
    }
}
