package master.models;

import lombok.Getter;
import lombok.NonNull;

public class FileResult {

    @Getter
    @NonNull
    private String commitHashCode;

    @Getter
    @NonNull
    private String fileName;

    @Getter
    private double result;
}
