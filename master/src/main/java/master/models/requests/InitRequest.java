package master.models.requests;

import lombok.Getter;

import javax.validation.constraints.NotNull;

/**
 * Request to initialize node.
 */
public class InitRequest {

    /**
     * The path of the repository to clone.
     */
    @Getter
    @NotNull
    private String repositoryPath;

    /**
     * An unique name id to identify the current execution.
     */
    @Getter
    @NotNull
    private String name;
}
