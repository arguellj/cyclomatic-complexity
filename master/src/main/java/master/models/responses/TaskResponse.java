package master.models.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

/**
 * Response to a task request.
 */
@AllArgsConstructor
public class TaskResponse {

    /**
     * Commit of task to execute.
     */
    @Getter
    private String commitHashCode;
}
