package master.models.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.Set;

@AllArgsConstructor
public class WholeCommitResponse {

    @Getter
    @NotNull
    public Set<String> fileNames;
}
