package master.models.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;

/**
 * Request to awake worker nodes.
 */
@AllArgsConstructor
public class AwakeWorkerRequest {

    /**
     * The path to the repository to clone.
     */
    @NotNull
    @Getter
    private String repositoryPath;

    /**
     * An unique name id for the current execution.
     */
    @Getter
    @NotNull
    private String name;
}
