package master.models.responses;

import lombok.Getter;
import lombok.NonNull;

/**
 * Response to a task request. This task consist on process a file with {@param fileName} in a specific commit.
 */
public class FileTaskResponse extends TaskResponse {

    /**
     * File name to process.
     */
    @NonNull
    @Getter
    private String fileName;

    public FileTaskResponse(String commitHashCode, String fileName) {
        super(commitHashCode);
        this.fileName = fileName;
    }
}
