package master.models;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.jgit.diff.DiffEntry;
import versioncontrol.models.FileChange;

public class FileHistory extends FileChange {

    @Getter
    @Setter
    private double result;

    @Getter
    @Setter
    private boolean hasBeenProcessed;

    public FileHistory(String fileName, DiffEntry.ChangeType type) {
        super(fileName, type);
        this.hasBeenProcessed = type == DiffEntry.ChangeType.DELETE;
        this.result = 0;
    }
}
