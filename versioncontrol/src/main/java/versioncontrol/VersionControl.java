package versioncontrol;

import versioncontrol.models.FileChange;

import java.util.Set;
import java.util.Stack;

public interface VersionControl {

    /**
     * Checkout a repository in the commit specified.
     * @param hashCode the hash of the desired commit to checkout
     */
    void checkoutRepository(final String hashCode);

    /**
     * Checkout a file in a commit specified.
     * @param fileName the file to checkout
     * @param hashCode the hash of the desired commit to checkout
     * @return
     */
    String checkoutFile(final String fileName, final String hashCode);

    /**
     * Clone a reposity
     * @param remotePath the path of the repository to clone
     * @param name the name of the folder where to keep the local copy
     */
    void cloneRepository(final String remotePath, final String name);

    /**
     * Returns the names of all the files in the current local repository.
     * @return set of file names
     */
    Set<String> getFileNames();

    Set<FileChange> getFilesChanged();

    /**
     * Pull the repository cloned.
     */
    void pullRepository();

    /**
     * Return the hash code of the commit where the HEAD is.
     * @return
     */
    String getCommitID();

    /**
     * Get all the hash commits for the current repository.
     * @return stack of commits's hash code
     */
    Stack<String> getAllCommitIds();

}
