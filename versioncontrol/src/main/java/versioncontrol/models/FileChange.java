package versioncontrol.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.eclipse.jgit.diff.DiffEntry;

@AllArgsConstructor
public class FileChange {

    @Getter
    private String fileName;

    @Getter
    private DiffEntry.ChangeType type;

    @Override
    public int hashCode() {
        return this.fileName.hashCode();
    }
}
