package versioncontrol;

import lombok.extern.log4j.Log4j;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathSuffixFilter;
import org.eclipse.jgit.util.io.DisabledOutputStream;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import versioncontrol.models.FileChange;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Git Version Control implementation.
 */
@Component
@Log4j
public final class GitVersionControl implements VersionControl {

    static final String FILTER_EXTENSION = ".java";

    /**
     * Git API.
     */
    private Git git;

    /**
     * The root path to where to keep local repositories.
     */
    @Value("${app.repository.local.path}")
    private String localRootPath;

    /**
     * Folder where is kept the local Repository.
     */
    private String repositoryFolder;

    /**
     * Local repository copy.
     */
    private Repository localRepository;

    @Override
    public synchronized void checkoutRepository(final String hashCode) {
        try {
            this.git.checkout().setName(hashCode).call();
        } catch (GitAPIException e) {
            log.error(e);
        }
    }

    @Override
    public synchronized String checkoutFile(String fileName, String hashCode) {
        try {
            git.checkout().setStartPoint(hashCode).addPath(fileName).call();
        } catch (GitAPIException e) {
            log.error(e);
        }

        return this.repositoryFolder + File.separator + fileName;
    }

    @Override
    public void cloneRepository(final String remotePath, final String name) {
        this.repositoryFolder = this.localRootPath + name;
        try {
            this.localRepository = new FileRepository(this.repositoryFolder + "/.git");
        } catch (IOException e) {
            log.error(e);
        }

        this.git = new Git(this.localRepository);

        File folder = new File(repositoryFolder);

        if (folder.exists()) {
            try {
                FileUtils.deleteDirectory(folder);
            } catch (IOException e) {
                log.error(e);
            }
        }

        log.info(String.format("Creating folder '%s' to clone repository '%s'.", this.repositoryFolder, remotePath));
        folder.mkdir();

        try {
            Git.cloneRepository()
                    .setURI(remotePath)
                    .setDirectory(new File(this.repositoryFolder))
                    .call();
        } catch (GitAPIException e) {
            log.error(e);
        }
    }

    @Override
    public synchronized Set<String> getFileNames() {
        Set<String> fileNames = new HashSet<String>();

        // a RevWalk allows to walk over commits based on some filtering that is defined
        RevWalk walk = new RevWalk(this.localRepository);

        ObjectId commitId = null;
        try {
            commitId = this.localRepository.resolve(Constants.HEAD);
        } catch (IOException e) {
            log.error(e);
        }

        RevCommit commit = null;
        try {
            commit = walk.parseCommit(commitId);
        } catch (IOException e) {
            log.error(e);
        }
        RevTree tree = commit.getTree();
        log.debug("Repository tree: " + tree);

        // now use a TreeWalk to iterate over all files in the Tree recursively
        TreeWalk treeWalk = new TreeWalk(this.localRepository);
        try {
            treeWalk.addTree(tree);
        } catch (IOException e) {
            log.warn(e);
        }

        treeWalk.setRecursive(true);
        // Filter to only get .java files
        treeWalk.setFilter(PathSuffixFilter.create(GitVersionControl.FILTER_EXTENSION));

        try {
            while (treeWalk.next()) {
                fileNames.add(treeWalk.getPathString());
            }
        } catch (IOException e) {
            log.warn(e);
        }

        return fileNames;
    }

    @Override
    public Set<FileChange> getFilesChanged() {
        ObjectReader reader = git.getRepository().newObjectReader();
        CanonicalTreeParser oldTreeIterator = new CanonicalTreeParser();
        CanonicalTreeParser newTreeIterator = new CanonicalTreeParser();

        ObjectId oldTree = null;
        ObjectId newTree = null;
        try {
            newTree = this.localRepository.resolve("HEAD^{tree}");
            oldTree = this.localRepository.resolve("HEAD~1^{tree}");

            if (newTree != null) newTreeIterator.reset(reader, newTree);
            if (oldTree != null) oldTreeIterator.reset(reader, oldTree);
        } catch (IOException e) {
            log.error(e);
        }

        DiffFormatter diffFormatter = new DiffFormatter(DisabledOutputStream.INSTANCE);
        diffFormatter.setRepository(this.localRepository);
        diffFormatter.setPathFilter(PathSuffixFilter.create(GitVersionControl.FILTER_EXTENSION));

        List<DiffEntry> entries = null;
        try {
            entries = diffFormatter.scan(oldTreeIterator, newTreeIterator);
        } catch (IOException e) {
            log.error(e);
        }

        Set<FileChange> changes = new HashSet<>();
        for (DiffEntry entry : entries) {
            FileChange fileChange = new FileChange(entry.getNewPath(), entry.getChangeType());
            changes.add(fileChange);
        }

        return changes;
    }

    @Override
    public synchronized void pullRepository() {
        try {
            this.git.pull().call();
        } catch (GitAPIException e) {
            log.error(e);
        }
    }

    @Override
    public synchronized String getCommitID() {
        ObjectId commitId = null;
        try {
            commitId = this.localRepository.resolve(Constants.HEAD);
        } catch (IOException e) {
            log.error(e);
        }

        return commitId.toString();
    }

    @Override
    public synchronized Stack<String> getAllCommitIds() {
        Iterator<RevCommit> iterator = null;
        Stack<String> commits = new Stack();

        try {
            iterator = this.git.log().call().iterator();
        } catch (GitAPIException e) {
            log.error(e);
        }

        while (iterator.hasNext()) {
            commits.push(iterator.next().getName());
        }

        return commits;
    }
}
