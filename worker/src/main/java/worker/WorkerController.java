package worker;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import worker.models.FileJob;
import worker.models.requests.InitRequest;
import worker.services.WorkerService;

import javax.validation.Valid;

import java.net.URI;

import static org.springframework.http.ResponseEntity.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@Log4j2
public class WorkerController {

    @Autowired
    @Qualifier("Slave")
    private WorkerService service;

    @RequestMapping(method = POST, value = "/init")
    public ResponseEntity<Object> init(@Valid @RequestBody final InitRequest request) {
        this.service.init(request.getRepositoryPath(), request.getName());
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{name}")
                .buildAndExpand(request.getName()).toUri();
        return created(location).build();
    }

    @RequestMapping(method = POST, value = "/process")
    public ResponseEntity<Object> process(@Valid @RequestBody final FileJob job) {
        this.service.processJob(job);
        return ok().build();
    }

    @RequestMapping(method = GET, value = "/greeting")
    public String greeting() {
        log.info("Greeting...");
        return "hello";
    }
}
