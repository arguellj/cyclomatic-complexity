package worker.models;

import lombok.Getter;
import lombok.NonNull;

/**
 * Job task of a file in a specific commit.
 */
public class FileJob {

    @NonNull
    @Getter
    private String fileName;

    @NonNull
    @Getter
    private String commitHashCode;
}
