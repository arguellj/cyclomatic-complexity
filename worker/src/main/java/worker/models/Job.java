package worker.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

@AllArgsConstructor
public class Job {

    @NonNull
    @Getter
    private String commitHashCode;
}
