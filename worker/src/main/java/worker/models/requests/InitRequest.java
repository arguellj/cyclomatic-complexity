package worker.models.requests;

import lombok.Getter;
import javax.validation.constraints.NotNull;

public class InitRequest {
    @NotNull
    @Getter
    private String repositoryPath;

    @Getter
    @NotNull
    private String name;
}
