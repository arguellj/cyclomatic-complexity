package worker.models.requests;

import lombok.Getter;

import javax.validation.constraints.NotNull;

public class ProcessRequest {

    @Getter
    @NotNull
    private String fileName;

    @Getter
    private String commitHashCode;
}
