package worker.models.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class ProcessResponse {

    @Getter
    private String commitHashCode;

    @Getter
    private String fileName;

    @Getter
    private double result;
}
