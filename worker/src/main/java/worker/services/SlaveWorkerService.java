package worker.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import worker.models.FileJob;
import worker.models.responses.ProcessResponse;

import java.util.concurrent.CompletableFuture;

@Service
@Log4j2
@Qualifier("Slave")
public class SlaveWorkerService extends WorkerService {
    @Override
    public void processJob(FileJob job) {
        CompletableFuture.runAsync(() -> super.processJob(job));
    }

    @Override
    void processResult(ProcessResponse result) {
        this.sendResult(result);
    }

}
