package worker.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import worker.models.FileJob;
import worker.models.responses.ProcessResponse;

import java.util.concurrent.CompletableFuture;

@Service
@Log4j2
@Qualifier("Stealing")
public class StealingWorkerService extends WorkerService {

    @Override
    public void init(String repositoryPath, String folderName) {
        super.init(repositoryPath, folderName);

        for (int cores = Runtime.getRuntime().availableProcessors(); cores > 0; cores--) {
            CompletableFuture.runAsync(this::getJob);
        }
    }

    @Override
    void processResult(ProcessResponse result) {
        CompletableFuture.runAsync(this::getJob);
        this.sendResult(result);
    }

    private void getJob() {
        log.debug("Request new job..");
        final String uri = masterBaseUrl + "/repository";
        RestTemplate restTemplate = new RestTemplate();
        FileJob result = restTemplate.getForObject(uri, FileJob.class);
        log.debug(String.format("Get job file: '%s' - commit: '%s'", result.getFileName(), result.getCommitHashCode()));

        if (result.getCommitHashCode().isEmpty()) {
            log.info("No more jobs to processed. Terminating...");
            return;
        }

        this.processJob(result);
    }
}
