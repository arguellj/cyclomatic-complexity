package worker.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;
import versioncontrol.VersionControl;
import worker.models.FileJob;
import worker.models.responses.ProcessResponse;
import worker.processor.IProcessorFactory;
import worker.processor.Processor;

import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Worker service to process files from a repository,
 */
@Log4j2
public abstract class WorkerService {

    /**
     * Factory to create processors.
     */
    @Autowired
    IProcessorFactory processorFactory;

    /**
     * Repository where get the files to process.
     */
    @Autowired
    VersionControl repository;

    /**
     * The base URL of the master node.
     */
    @Value("${app.master.baseUrl}")
    String masterBaseUrl;

    /**
     * Map of files and locks to avoid process the file with the same name at the same.
     * Key          Value
     * FileName -> {@link Lock}
     */
    ConcurrentHashMap<String, Lock> fileNamesBeingProcessed = new ConcurrentHashMap();

    /**
     * Count down to wait to clone the repository before start to process.
     */
    CountDownLatch countDown = new CountDownLatch(1);

    /**
     * Initialize the worker node service.
     * @param repositoryPath the path of the repository to clone.
     * @param folderName the name of the folder to keep a local copy.
     */
    public void init(String repositoryPath, String folderName) {
        this.repository.cloneRepository(repositoryPath, folderName);
        countDown.countDown();
    }

    /**
     * Process the job assigned.
     * @param job the job to process.
     */
    public void processJob(FileJob job) {
        try {
            // Wait until the repository is cloned.
            countDown.await();
        } catch (InterruptedException e) {
            log.warn(e);
        }
        log.info(String.format("Processing file '%s' in commit '%s'", job.getFileName(), job.getCommitHashCode()));

        // Lock file until the process has finished
        this.fileNamesBeingProcessed.putIfAbsent(job.getFileName(), new ReentrantLock());
        this.fileNamesBeingProcessed.get(job.getFileName()).lock();
        log.debug(String.format("Got lock for %s", job.getFileName()));
        final String pathToFile = this.repository.checkoutFile(job.getFileName(), job.getCommitHashCode());
        log.debug(String.format("Checkout file %s", job.getFileName()));
        final Processor processor = this.processorFactory.createProcessor();

        double result = 0.0;
        try {
            CompletableFuture.runAsync(() -> processor.execute(pathToFile)).get(5, TimeUnit.SECONDS);
            result = processor.getResult();
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            log.warn("Killing Processor because it has taken too long.", e);
        }

        this.fileNamesBeingProcessed.get(job.getFileName()).unlock();
        log.debug(String.format("Release lock for %s", job.getFileName()));

        log.info(String.format("Returning result %.2f", result));
        final ProcessResponse resultResponse = new ProcessResponse(
                job.getCommitHashCode(), job.getFileName(), result);
        this.processResult(resultResponse);
    }

    /**
     * Send the result to the master node.
     * @param response the response to send.
     */
    void sendResult(ProcessResponse response) {
        log.debug("Sending result...");
        final String uri = masterBaseUrl + "/repository";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForLocation(uri, response);
    }

    /**
     * Process the result of obtained from process the job.
     * @param result the result of the process job
     */
    abstract void processResult(ProcessResponse result);
}
