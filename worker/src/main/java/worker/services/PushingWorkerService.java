package worker.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import worker.models.FileJob;
import worker.models.responses.ProcessResponse;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

@Service
@Log4j2
@Qualifier("Pushing")
public class PushingWorkerService extends WorkerService {

    LinkedBlockingQueue<FileJob> pendingWork = new LinkedBlockingQueue();

    @Override
    public void init(String repositoryPath, String folderName) {
        super.init(repositoryPath, folderName);

        for (int cores = Runtime.getRuntime().availableProcessors(); cores > 0; cores--) {
            CompletableFuture.runAsync(this::getJob);
        }
    }

    @Override
    void processResult(ProcessResponse result) {
        CompletableFuture.runAsync(this::getJob);
        this.sendResult(result);
    }

    @Override
    public void processJob(FileJob job) {
        try {
            this.pendingWork.put(job);
        } catch (InterruptedException e) {
            log.warn(e);
        }
    }

    private void getJob() {
        log.debug("Requesting new job..");

        FileJob job = null;
        try {
            job = this.pendingWork.poll(30, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            log.warn(e);
        }

        if (job == null) {
            log.info("No more jobs to processed. Terminating...");
            return;
        }

        super.processJob(job);
    }
}
