package worker.processor.cc;

import java.util.Stack;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.antlr.v4.runtime.RuleContext;

import worker.processor.cc.grammar.JavaParser;
import worker.processor.cc.grammar.JavaParser.ClassDeclarationContext;
import worker.processor.cc.grammar.JavaParser.ConstructorDeclarationContext;
import worker.processor.cc.grammar.JavaParser.EnumDeclarationContext;
import worker.processor.cc.grammar.JavaParser.ExpressionContext;
import worker.processor.cc.grammar.JavaParser.MethodDeclarationContext;
import worker.processor.cc.grammar.JavaParser.StatementContext;
import worker.processor.cc.grammar.JavaParserBaseVisitor;

/**
 * Tree Visitor to measure the cyclomatic complexity.
 */
@Log4j2
public class CyclomaticComplexityVisitor extends JavaParserBaseVisitor<Integer> {

    private Stack<Entry> entryStack = new Stack<>();

    @Getter
    private double cyclomaticComplexitySourceCode = 0;

    @Override
    public Integer visitStatement(StatementContext ctx) {
        RuleContext rc = ctx.getPayload();

        if (rc != null) {
            if (ctx.getTokens(JavaParser.ELSE).size() > 0 ||
                    ctx.getTokens(JavaParser.IF).size() > 0 ||
                    ctx.getTokens(JavaParser.WHILE).size() > 0 ||
                    ctx.getTokens(JavaParser.FOR).size() > 0 ||
                    ctx.getTokens(JavaParser.CATCH).size() > 0 ||
                    ctx.getTokens(JavaParser.SWITCH).size() > 0 ||
                    ctx.getTokens(JavaParser.DO).size() > 0) {

                this.entryStack.peek().bumpDecisionPoints();
                return super.visitStatement(ctx);
            }
        }

        return super.visitStatement(ctx);
    }

    @Override
    public Integer visitExpression(ExpressionContext ctx) {
        //expression '?' expression ':' expression
        if (ctx.getTokens(JavaParser.QUESTION).size() > 0) {
            this.entryStack.peek().bumpDecisionPoints();
        }

        return super.visitExpression(ctx);
    }

    @Override
    public Integer visitMethodDeclaration(MethodDeclarationContext ctx) {
        this.entryStack.push(new Entry(ctx));
        Integer res = super.visitMethodDeclaration(ctx);

        Entry methodEntry = this.entryStack.pop();

        int methodDecisionPoints = methodEntry.decisionPoints;

        Entry classEntry = this.entryStack.peek();
        classEntry.methodCount++;
        classEntry.bumpDecisionPoints(methodDecisionPoints);

        if (methodDecisionPoints > classEntry.highestDecisionPoints) {
            classEntry.highestDecisionPoints = methodDecisionPoints;
        }

        return res;
    }

    @Override
    public Integer visitClassDeclaration(ClassDeclarationContext ctx) {
        this.entryStack.push(new Entry(ctx));
        Integer res = super.visitClassDeclaration(ctx);

        Entry classEntry = entryStack.peek();

        log.debug(String.format("overall methods count: %d\n", classEntry.methodCount));

        double avgCC = classEntry.methodCount != 0 ? classEntry.decisionPoints * 1f / classEntry.methodCount : 0;

        this.cyclomaticComplexitySourceCode += avgCC;

        return res;
    }

    @Override
    public Integer visitConstructorDeclaration(ConstructorDeclarationContext ctx) {
        this.entryStack.push(new Entry(ctx));
        Integer res = super.visitConstructorDeclaration(ctx);
        Entry constructorEntry = this.entryStack.pop();

        int constructorDecisionPointCount = constructorEntry.decisionPoints;
        Entry classEntry = this.entryStack.peek();
        classEntry.methodCount++;
        classEntry.decisionPoints += constructorDecisionPointCount;

        if (constructorDecisionPointCount > classEntry.highestDecisionPoints) {
            classEntry.highestDecisionPoints = constructorDecisionPointCount;
        }

        return res;
    }

    @Override
    public Integer visitEnumDeclaration(EnumDeclarationContext ctx) {
        this.entryStack.push(new Entry(ctx));
        Integer res = super.visitEnumDeclaration(ctx);
        Entry classEntry = this.entryStack.pop();

        return res;
    }
}


class Entry {
    private Object node;
    public int decisionPoints = 1;
    public int highestDecisionPoints;
    public int methodCount;

    Entry(Object node) {
        this.node = node;
    }

    public void bumpDecisionPoints() {
        this.decisionPoints++;
    }

    public void bumpDecisionPoints(final int size) {
        this.decisionPoints += size;
    }

    public int getComplexityAverage() {
        return (double) this.methodCount == 0 ? 1
                : (int) Math.rint((double) this.decisionPoints / (double) this.methodCount);
    }
}
