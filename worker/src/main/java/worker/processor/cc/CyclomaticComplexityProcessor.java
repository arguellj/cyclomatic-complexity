package worker.processor.cc;

import lombok.extern.log4j.Log4j2;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import worker.processor.Processor;
import worker.processor.cc.grammar.JavaLexer;
import worker.processor.cc.grammar.JavaParser;
import worker.processor.exceptions.FileNotFoundException;

import java.io.IOException;

/**
 * Process a file by calculating its Cyclomatic Complexity.
 */
@Service
@Log4j2
@Scope(value="prototype")
public class CyclomaticComplexityProcessor implements Processor {

    /**
     * Tree visitor to visit every node given by a parser.
     */
    private CyclomaticComplexityVisitor visitor;

    @Override
    public void execute(final String fileName) {
        Lexer lexer = null;
        try {
            lexer = new JavaLexer(CharStreams.fromFileName(fileName));
        } catch (IOException e) {
            log.warn(e);
            throw new FileNotFoundException("Error: " + e.getMessage(), e);
        }

        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final JavaParser parser = new JavaParser(tokens);
        final ParserRuleContext tree = parser.compilationUnit();
        this.visitor = new CyclomaticComplexityVisitor();
        this.visitor.visit(tree);
    }

    @Override
    public double getResult() {
        return this.visitor.getCyclomaticComplexitySourceCode();
    }
}
