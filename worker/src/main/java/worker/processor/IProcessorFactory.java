package worker.processor;

import org.springframework.beans.factory.annotation.Lookup;

public interface IProcessorFactory {

    @Lookup
    Processor createProcessor();
}
