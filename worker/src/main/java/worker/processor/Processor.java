package worker.processor;

/**
 * Process a file.
 */
public interface Processor {
    /**
     * Process the file {@param filename}
     * @param fileName the name of the file to process.
     */
    void execute(String fileName);

    /**
     * Get result of execution
     * @return the result of the execution for the file.
     */
    double getResult();
}
