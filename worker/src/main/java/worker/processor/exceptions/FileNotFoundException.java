package worker.processor.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Execution where the file specified does not exist.
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="No such file.")
public class FileNotFoundException extends RuntimeException {
    public FileNotFoundException() { super(); }
    public FileNotFoundException(String message) { super(message); }
    public FileNotFoundException(String message, Throwable cause) { super(message, cause); }
    public FileNotFoundException(Throwable cause) { super(cause); }
}
