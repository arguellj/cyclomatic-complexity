# Cyclomatic Complexity Task

## Summary

This is the seconf assignment for the module CS4400-Scalable Computing for the Master in Science of Computer Science at Trinity College Dublin.

This project calculates the cyclomatic complexity of Java files in a Git Reposiry by implementing the three Communication Patterns discussed in [Communication Patterns in Cloud Haskell](http://www.well-typed.com/blog/71/). This Communication Patterns can be activated by configuration one at the time per execution.
The results of the execution are saved to a Excel file, which includes the processing times and results of all the commits, and the total processing time and result of the repository.

## How do I get set up

### Application Dependencies

The project is built in Java 8. It uses [Maven](https://maven.apache.org/) as project management tool.
The following dependencies are included using Maven:

* [SpringBoot](https://projects.spring.io/spring-boot/) - Dependency Injection and RESTful Framework - [Maven Repository](https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-web)
* [JUnit](http://junit.org/junit4/) - Unit Testing Framework - [Maven Repository](https://mvnrepository.com/artifact/junit/junit)
* [Project Lombok](https://projectlombok.org/) - Library to simplify development - [Maven Repository](https://mvnrepository.com/artifact/org.projectlombok/lombok-maven)
* [Log4j2](https://logging.apache.org/log4j/2.x/) - Logging Utility - [Maven Repository](https://mvnrepository.com/artifact/log4j/log4j)
* [JGit](https://github.com/eclipse/jgit) - Git Java Library [Maven Repository](https://mvnrepository.com/artifact/org.eclipse.jgit/org.eclipse.jgit/4.9.0.201710071750-r)
* [Antlr](http://www.antlr.org/) - Parse Generator - [Maven Repository](https://mvnrepository.com/artifact/org.antlr/antlr4-runtime/4.7)
* [JsonPath](https://github.com/json-path/JsonPath) - Json Library - [Maven Repository](https://mvnrepository.com/artifact/com.jayway.jsonpath/json-path/2.4.0)
* [POI](https://poi.apache.org/) - Microsoft Document API - [Maven Repository](https://mvnrepository.com/artifact/org.apache.poi/poi/3.17)

### Environment Dependencies

* [Maven](https://maven.apache.org/). If the build and execution are done locally. More information in section [Compile and run locally](#compile-and-run-locally).

* [Docker](https://www.docker.com). If the build and execution are done by using Docker. More information in section [Compile and run with Docker](#compile-and-run-with-docker).

### How to compile and run

The project can be compiled and run in two ways, locally or by using Docker. The following sections described these alternatives.

#### Compile and run locally

In order to run the project, it has to be compiled by using Maven with the following command:

> ./mvnw clean package

then, to execute it use the command:

> ./mvnw spring-boot:run

#### Compile and run with Docker

In order to run the project, it has to be compiled by using Maven with the following command:

> ./compile-with-docker

then, to execute it use the command:

> ./start-with-docker.sh

### How to start an execution

Send the following to the RESTful master node:

* **Http Method:** `POST`
* **Endpoint:** `/init`
* **Body:** `application/json`

```json
{
    "repositoryPath": <string>, // Http repository path
    "name": <string>            // Unique identifier for the execution. The saved file will be named as it.
}
```

### Benchmarking

A series of experiments were done by using Slave-Master, Work-Pushing and Work-Stealing in the following GitHub repositories:

* [Java Design Patterns](https://github.com/iluwatar/java-design-patterns)
* [JavaRx](https://github.com/ReactiveX/RxJava)
* [Maven](https://github.com/apache/maven)

The results can be found in this repository in the folder `results`.

The report is also in this repository in a pdf file named `Cyclomatic Complexity Report.pdf`.

### Clarifications

#### Processing of repository

The repository was divided in subcomputations. Each subcomputation consists of calculating the Cyclomatic Complexity of one file in specific commit. For all the communcation patterns the repository is read from the oldest commit to the newest, then by using a stack every commit is recovered and **only** the modified files are intended to be processed (by pushing them in a queue or send them to the workers depending of the communication pattern activated). This is the Map step of the Map-Reduce technique.


Then, all the results are processed asynhronously and when all the files of all the commits has been processed, the reduce step of the Map-Reduce technique starts. This is the sum of the results for all the commits to get the total result for the repository. In the case of the commits that have unmodified files, the Cyclomatic Complexity of those unmodifed files are recovered from the previous commit as part of the reduce step. This is possible because each commit tracks the result of the modified files that were processed. When a commit get the result of the previous commit, it also extracts the records of the unmodifed files that belongs to the current commit. The purpose of this reallocation is to allow the next commit to recover the results of unmodified files from this commit. So basically, the unmodifed files are being moved from one commit to the other to allow newer commits to access previous results. As result, a lot of files do not have to be processed if they were aleady processed in previous commits because they are exaclty the same file. This cache mechanism speed up the performance of the calculation by avoiding unneccessary communcations between master and workers.

### Implementation of communcation patterns

The implementation of the communcation patterns follow exactly the indications of the article [Communication Patterns in Cloud Haskell](http://www.well-typed.com/blog/71/)].

### Author Declaration

I have read and I understand the plagiarism provisions in the General Regulations of the University Calendar for the current year, found at: [http://www.tcd.ie/calendar](http://www.tcd.ie/calendar). I have also completed the Online Tutorial on avoiding plagiarism ‘Ready, Steady, Write’, located at [http://tcd-ie.libguides.com/plagiarism/ready-steady-write](http://tcd-ie.libguides.com/plagiarism/ready-steady-write) I declare that this assignment, together with any supporting artefact is offered for assessment as my original and unaided work, except in so far as any advice and/or assistance from any other named person in preparing it and any reference material used are duly and appropriately acknowledged.

### FAQ

**Q.** Which Maven version was used to build the project?

**A:** Maven 3.5.2

**Q.** How do I install Maven?

**A:** Please follow the steps mentioned in [Maven Installation Guide](https://maven.apache.org/install.html).

**Q.** I get error message: *Permission denied* when try to run any script.

**A:** Execute in the terminal console:

> chmod 755 start-with-docker.sh compile-with-docker.sh

**Q.** What does contain the Docker image?

**A:** It contains a lightweight linux distribution called alpine. The [docker image](https://hub.docker.com/_/maven/) used contains the JDK 8 and Maven 3.5.2, which are the dependencies of the applications. Then, Maven takes care of the specific dependencies of the application mentioned in the [Application Dependencies Section](#application-dependencies).

## Who do I talk to

* Jeancarlo Arguello Calvo - arguellj@tcd.ie
* StudentID: 17307291
* Trinity College Dublin, The University of Dublin